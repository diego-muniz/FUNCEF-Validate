(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-validate.controller', []);
    angular.module('funcef-validate.service', []);
    angular.module('funcef-validate.component', []);
    angular.module('funcef-validate.directive', []);

    angular
    .module('funcef-validate', [
      'funcef-validate.controller',
      'funcef-validate.service',
      'funcef-validate.component',
      'funcef-validate.directive'
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-validate.controller')
        .controller('NgfValidateController', NgfValidateController);

    NgfValidateController.$inject = ['$scope', '$timeout', '$element', '$compile', '$q'];

    /* @ngInject */
    function NgfValidateController($scope, $timeout, $element, $compile, $q) {
        var vm = this;
        vm.errors = {};
        vm.cntOrder = 0;
        vm.isObjectEmpty = isObjectEmpty;

        vm.messages = {
            default: 'Campo inválido!',
            required: 'Campo obrigatório!',
            cpf: 'CPF informado é inválido!',
            cnpj: 'CNPJ informado é inválido!',
            email: 'E-mail informado é inválido!',
            number: 'Número informado é inválido!',
            date: 'Data informada é inválido!',
            dateTime: 'Data e Hora informados são inválidos!',
            time: 'Hora Informada é inválida!',
            url: 'URL informada é inválido!',
            minlength: 'Mínimo {p} caracteres!',
            maxlength: 'Máximo {p} caracteres!',
            mesAno: 'Período informado é invalido!',
            minValue: 'O valor mínimo é {p}!',
            maxValue: 'O valor máximo é {p}!',
            minSelected: 'Selecione no mínimo "{p}" !',
            maxSelected: 'Selecione no máximo "{p}" !'
        };

        vm.functionValidator = {
            cpf: validaCPF,
            cnpj: validaCNPJ,
            email: validaEmail,
            date: validaDate,
            dateTime: validaDatetime,
            time: validaTime,
            number: validaNumber,
            url: validaUrl,
            mesAno: validaMeAno,
            minValue: validaMinValue,
            maxValue: validaMaxValue,
            minSelected: validaMinSelected,
            maxSelected: validaMaxSelected
        };

        vm.options = {
            classMessageError: 'text-danger bg-danger p-5',
            elementError: 'p',
            classAlertErros: 'alert alert-danger',
            classError: 'has-error',
            classSuccess: 'has-info',
            formGroup: '.form-group'
        };

        vm.attrs = {
            minValue: 'min-value',
            maxValue: 'max-value',
            minSelected: 'min-selected',
            maxSelected: 'max-selected',
            minValueType: 'minValue',
            maxValueType: 'maxValue',
            minSelectedType: 'minSelected',
            maxSelectedType: 'maxSelected',
            checkAll: 'check-all',
            condition: 'condition',
            custom: 'custom',
            message: 'message',
            classUiSelect: 'ui-select-search',
            classUiSelect2: 'ui-select-container',
            uiSelectContainer: '.ui-select-container',
            momentPickerInput: 'moment-picker-input',
            validateOrder: 'validate-order',
            zero: 'zero',
            info: 'info'
        };

        vm.attrsValidate = {
            minValue: vm.attrs.minValue,
            maxValue: vm.attrs.maxValue,
            minSelected: vm.attrs.minSelected,
            maxSelected: vm.attrs.maxSelected,
            minValueType: vm.attrs.minValueType,
            maxValueType: vm.attrs.maxValueType,
            minSelectedType: vm.attrs.minSelectedType,
            maxSelectedType: vm.attrs.maxSelectedType,
            custom: vm.attrs.custom
        };

        vm.alerts = {
            error: 'Existem errors no formulário. Verifique e tente novamente!',
            info: 'Os campos marcados com <span class="req">*</span> são de preenchimento obrigatório!</div>',
        };

        init();

        /////////

        function init() {
            $timeout(function () {
                addValidateField();
                addAlertInfo();
            }, 1000);
        }

        function addValidateField() {
            if (!$scope.name) {
                return false;
            }

            angular.forEach($scope.name.$$controls, function (control, key) {
                if (control.$$element.is(':text') || control.$$element.is(':password')) {
                    var timer = null;
                    control.$$element.keyup(function (event) {
                        clearTimeout(timer);
                        timer = setTimeout(function () {
                            validItemByBind(event);
                        }, 500);
                    });
                }

                control.$$element.blur(function (event) {
                    validItemByBind(event);
                });

                addValidateOrder(control.$$element);
                addIndicatorRequired(control);
            });

            angular.element(vm.attrs.uiSelectContainer + ' a').click(function (event) {
                validField(angular.element(event.currentTarget).closest(vm.attrs.uiSelectContainer));
                verifyIndicatorRequired();
            });

            $scope.name.$$element.submit(function () {
                validAllFields();
            });

            $scope.name.valid = function () {
                validAllFields();
                return $scope.name.$valid && !Object.keys(vm.errors).length;
            };
        }

        function validItemByBind(event) {
            $timeout(function () {
                validField(angular.element(event.currentTarget));
                verifyIndicatorRequired();
            }, 200);
        }

        function validAllFields() {
            angular.forEach($scope.name.$$controls, function (control, key) {
                validField(angular.element(control.$$element[0]));
            });
            showListError();
        }

        function validField(field) {
            removeValidators(field);

            if (isUiSelect(field)) {
                field = field.closest(vm.attrs.uiSelectContainer);
            }

            field.order = field.closest('[' + vm.attrs.validateOrder + ']').attr(vm.attrs.validateOrder);

            $timeout(function () {
                validaFieldItem(field);
            });
        }

        function validaFieldItem(field) {
            var typeError = '';
            removeValidityField(field);

            var errorAngular = existErrorsAngular(field);

            if (existErrorsRequired(field)) {
                addErrorElement(field, 'required');

            } else if (errorAngular) {
                addErrorElement(field, errorAngular);

            } else if (existErrorsValidatorsAttr(field)) {

            } else if (existErrorsMinValue(field)) {
                addErrorElement(field, vm.attrs.minValueType);

            } else if (existErrorsMaxValue(field)) {
                addErrorElement(field, vm.attrs.maxValueType);

            } else if (existErrorsMinSelected(field)) {
                addErrorElement(field, vm.attrs.minSelectedType);

            } else if (existErrorsMaxSelected(field)) {
                addErrorElement(field, vm.attrs.maxSelectedType);

            } else {
                addSuccessElement(field);
            }
        }

        function validator(type, field) {
            if (type == 'custom') {
                var funct = field.attr(vm.attrs.condition);
                if (funct) {
                    var ctrl = funct.split('.');
                    var customValidate = $scope.$parent[ctrl[0]][ctrl[1]];
                    return customValidate();
                }
            } else if (vm.functionValidator[type]) {
                if (field.val() != '') {
                    return vm.functionValidator[type](field.val(), field);
                }
                return true;
            }
        }

        function addSuccessElement(field, typeError) {
            var name = getNameByElement(field);

            if (!$scope.name[name] || !$scope.name[name].$valid) {
                return false;
            }

            var fieldParent = field.parent();

            if (isRadioOrCheckbox(field)) {

            } else if (isUiSelect(field)) {
                fieldParent = field.closest(vm.attrs.uiSelectContainer).parent();

            } else if (isMomentPicker(field)) {
                fieldParent = field.parent().parent();
            }

            fieldParent.addClass(vm.options.classSuccess);
            delete vm.errors[field.order];
        }

        function addErrorElement(field, typeError) {
            var textField = '';
            if (existMessageError(field)) {
                return false;
            }

            var message = getmessageError(field, typeError);
            var messageError = '<' + vm.options.elementError + ' class="form-validate ' + vm.options.classMessageError + '">'
                                   + message + '</' + vm.options.elementError + '>';

            if (field.closest(vm.options.formGroup).length) {
                field.closest(vm.options.formGroup)
                    .append(messageError)
                    .addClass(vm.options.classError);

            } else if (isMomentPicker(field)) {
                field.parent()
                    .parent()
                    .append(messageError)
                    .parent()
                    .addClass(vm.options.classError);

            } else if (isRadioOrCheckbox(field)) {
                field.parent()
                    .parent()
                    .append(messageError)
                    .parent()
                    .addClass(vm.options.classError);

            } else if (isUiSelect(field)) {
                field.parent()
                     .addClass(vm.options.classError)
                     .append(messageError);

            } else {
                field.parent()
                     .append(messageError)
                     .addClass(vm.options.classError);
            }

            if (isRadioOrCheckbox(field)) {
                textField = field.parent()
                    .parent()
                    .prev().text();
            } else {
                textField = angular.element('[for="' + field.attr('id') + '"]', $element).text();
            }

            textField = textField.replace('*', '');

            vm.errors[field.order] = {
                name: textField,
                message: message
            };

            setValidityField(field, typeError, false);
        }

        function showListError() {
            var html = '';
            var message = '<i class="fa fa-exclamation-circle" /><strong> Atenção:</strong> ' + vm.alerts.error;
            var button = '<button class="btn btn-default btn-sm pull-right" ng-click="vm.showErros = !vm.showErros"><i class="fa fa-eye" /></button>';

            $timeout(function () {
                html = '<div class="errors-validate ' + vm.options.classAlertErros + '" ng-show="!vm.isObjectEmpty(vm.errors)">'
                    + message + button + '<ul ng-show="vm.showErros">'
                    + '<li ng-repeat="(key, value) in vm.errors"><strong>{{value.name}}</strong>{{value.message}}</li>'
                    + '</ul></div>';

                if (!$element.find('.errors-validate').length) {
                    $element.prepend($compile(html)($scope));
                }
            });
        }

        function addAlertInfo() {
            if ($scope.name.$$element.attr(vm.attrs.info) == 'false') {
                return false;
            }

            var info = '<div class="alert alert-info" ng-show="vm.isObjectEmpty(vm.errors)">'
                     + '<i class="fa fa-info-circle"></i>'
                     + ' ' + vm.alerts.info;

            $element.before($compile(info)($scope));
        }

        function isObjectEmpty(obj) {
            return Object.keys(obj).length === 0;
        }

        function existMessageError(field) {
            var parent = field.parent();

            if (isRadioOrCheckbox(field) || isMomentPicker(field)) {
                parent = field.parent().parent();
            }

            return parent.find('.form-validate').length;
        }

        function getmessageError(field, typeError) {
            var mensagem = vm.messages[typeError];

            if (['minlength', 'maxlength'].indexOf(typeError) >= 0) {
                mensagem = mensagem.replace('{p}', field.attr('ng-' + typeError));
            }

            if ('minValue' == typeError) {
                mensagem = mensagem.replace('{p}', field.attr(vm.attrs.minValue));
            }

            if ('maxValue' == typeError) {
                mensagem = mensagem.replace('{p}', field.attr(vm.attrs.maxValue));
            }

            if ('minSelected' == typeError) {
                var fieldValid = field;
                if (isFieldCheckAll(field)) {
                    fieldValid = getElementByCheckAll(vm.attrs.minSelected, field);
                }

                mensagem = mensagem.replace('{p}', fieldValid.attr(vm.attrs.minSelected));
            }

            if ('maxSelected' == typeError) {
                var fieldValid = field;
                if (isFieldCheckAll(field)) {
                    fieldValid = getElementByCheckAll(vm.attrs.maxSelected, field);
                }
                mensagem = mensagem.replace('{p}', fieldValid.attr(vm.attrs.maxSelected));
            }

            if ("custom" == typeError) {
                mensagem = field.attr(vm.attrs.message);
            }

            if (!mensagem) {
                mensagem = vm.messages.default;
            }

            return mensagem;
        }

        function removeValidators(field) {
            var elementGroup = '';

            if (field.closest(vm.options.formGroup).length) {
                elementGroup = field.closest(vm.options.formGroup);

            } else {
                elementGroup = field.parent();

                if (isRadioOrCheckbox(field)) {
                    elementGroup = elementGroup.parent().parent();
                }

                if (isUiSelect(field)) {
                    elementGroup = field.closest(vm.attrs.uiSelectContainer).parent();
                }

                if (isMomentPicker(field)) {
                    elementGroup = field.parent().parent();
                }
            }

            elementGroup.removeClass(vm.options.classError)
                        .removeClass(vm.options.classSuccess);

            var validators = field.attr('validator');

            if (validators) {
                var types = validators.split(',');
                angular.forEach(types, function (type) {
                    setValidityField(field, type, true);
                });
            }

            angular.element('.form-validate', elementGroup).remove();

            //ajuste por causa do IE
            var values = Object.keys(vm.attrsValidate).map(function (e) {
                return vm.attrsValidate[e];
            });
            //

            angular.forEach($(field)[0].attributes, function (attr) {
                var nome = attr.name;
                if (values.indexOf(nome) >= 0) {
                    setValidityField(field, nome, true);
                }
            });
        }

        function setValidityField(field, typeError, value) {
            var name = getNameByElement(field);

            if (!$scope.name[name]) {
                var fieldName = field;
                if (isUiSelect(field)) {
                    fieldName = field.closest(vm.attrs.uiSelectContainer);
                }
                throw 'Elemento ' + fieldName.attr('ng-model') + ' não possui o atributo "name"';
            }
            $scope.name[name].$setValidity(typeError, value);
        }

        function removeValidityField(field) {
            if (field.attr(vm.attrs.minValue)) {
                setValidityField(field, vm.attrs.minValueType, true);
            }

            if (field.attr(vm.attrs.maxValue)) {
                setValidityField(field, vm.attrs.maxValueType, true);
            }

            if (field.attr(vm.attrs.minSelected)) {
                setValidityField(field, vm.attrs.minSelectedType, true);
            }

            if (field.attr(vm.attrs.maxSelected)) {
                setValidityField(field, vm.attrs.maxSelectedType, true);
            }

            if (field.attr(vm.attrs.condition)) {
                setValidityField(field, vm.attrs.custom, true);
            }
        }

        function isRadioOrCheckbox(field) {
            return ['radio', 'checkbox'].indexOf(field.attr('type')) >= 0;
        }

        function isFieldCheckAll(field) {
            return field.attr(vm.attrs.checkAll);
        }

        function isUiSelect(field) {
            return field.hasClass(vm.attrs.classUiSelect) || field.hasClass(vm.attrs.classUiSelect2);
        }

        function isMomentPicker(field) {
            return field.hasClass(vm.attrs.momentPickerInput);
        }

        function getElementByCheckAll(attrs, field) {
            return angular.element('[' + attrs + ']', field.parent().parent().parent());
        }

        function addIndicatorRequired(control) {
            if (control.$$attr.id) {
                var element = angular.element('[for="' + control.$$attr.id + '"]', $element);

                if (isRadioOrCheckbox(control.$$element)) {
                    var label = angular.element(control.$$element).parent().parent().prev();
                    if (label.is('label')) {
                        element = label;
                    }
                }

                angular.element('.req', element).remove();

                if (control.$$attr.required || !isNaN(parseInt(control.$$attr.minSelected))) {
                    element.append('<span class="req"> *</span>');
                }
            }
        }

        function addValidateOrder(field) {
            if (!field.hasClass(vm.attrs.classUiSelect)) {
                vm.cntOrder++;
                field.attr(vm.attrs.validateOrder, vm.cntOrder);
            }
        }

        function verifyIndicatorRequired() {
            angular.forEach($scope.name.$$controls, function (control, key) {
                addIndicatorRequired(control);
            });
        }

        function getNameByElement(field) {
            var name = field.attr('name');
            if (isUiSelect(field)) {
                name = field.closest(vm.attrs.uiSelectContainer).attr('name');
            }

            return name;
        }

        function getValidatorsField(field) {
            var validators = field.attr('validator');
            if (validators) {
                return validators.split(',');
            }
        }

        // Valida Exist Errors

        function existErrorsRequired(field) {
            var error = field.hasClass('ng-invalid-required');

            if (!error) {
                var types = getValidatorsField(field);
                if (types && types.indexOf('number') >= 0 && field.val() == 0 && field.attr('required') && field.val() != '') {
                    error = !field.attr(vm.attrs.zero) || field.attr(vm.attrs.zero) == 'false';
                }
            }

            return error;
        }

        function existErrorsValidatorsAttr(field) {
            var types = getValidatorsField(field);
            if (types) {
                angular.forEach(types, function (type) {
                    if (!validator(type, field)) {
                        addErrorElement(field, type);
                        return true;
                    }
                });
            }
        }

        function existErrorsAngular(field) {
            var name = getNameByElement(field);
            if ($scope.name[name] && $scope.name[name].$invalid) {
                var error = angular.copy($scope.name[name].$error);
                delete error['required'];
                var keys = Object.keys(error);
                return keys.length ? keys[0] : false;
            }
        }

        function existErrorsMinValue(field) {
            return field.attr(vm.attrs.minValue) && !validator('minValue', field);
        }

        function existErrorsMaxValue(field) {
            return field.attr(vm.attrs.maxValue) && !validator('maxValue', field);
        }

        function existErrorsMinSelected(field) {
            var attr = field.attr(vm.attrs.minSelected);
            var fieldValid = field;

            if (field.attr(vm.attrs.checkAll)) {
                attr = true;
                fieldValid = getElementByCheckAll(vm.attrs.minSelected, field);
            }

            return attr && !validator('minSelected', fieldValid);
        }

        function existErrorsMaxSelected(field) {
            var attr = field.attr(vm.attrs.maxSelected);
            var fieldValid = field;

            if (field.attr(vm.attrs.checkAll)) {
                attr = true;
                fieldValid = getElementByCheckAll(vm.attrs.minSelected, field);
            }

            return attr && !validator('maxSelected', fieldValid);
        }

        function toFloat(valor) {
            valor = valor.replace(/\./g, '');
            valor = valor.replace(/,/g, '.');
            return parseFloat(valor);
        }

        // Validações de campos

        function validaCPF(cpf) {
            var blacklist = [
                "00000000000",
                "11111111111",
                "22222222222",
                "33333333333",
                "44444444444",
                "55555555555",
                "66666666666",
                "77777777777",
                "88888888888",
                "99999999999",
                "12345678909",
                "00000000191"
            ];
            var add, rev;

            if (!cpf) return false;

            cpf = cpf.toString();
            cpf = cpf.replace(/[^\d]+/g, '');

            if (cpf == '') return false;

            if (blacklist.indexOf(cpf) > 0) return false;

            // Valida 1º digito
            add = 0;
            for (var i = 0; i < 9; i++) add += parseInt(cpf.charAt(i)) * (10 - i); rev = 11 - (add % 11);
            if (rev == 10 || rev == 11) rev = 0;
            if (rev != parseInt(cpf.charAt(9))) return false;

            // Valida 2º digito
            add = 0;
            for (var i = 0; i < 10; i++) add += parseInt(cpf.charAt(i)) * (11 - i); rev = 11 - (add % 11);
            if (rev == 10 || rev == 11) rev = 0;
            if (rev != parseInt(cpf.charAt(10))) return false;

            return true;
        }

        function validaCNPJ(cnpj) {
            cnpj = cnpj.replace(/[^\d]+/g, '');

            if (cnpj == '') return false;

            if (cnpj.length != 14)
                return false;

            // Elimina CNPJs invalidos conhecidos
            if (cnpj == "00000000000000" ||
                cnpj == "11111111111111" ||
                cnpj == "22222222222222" ||
                cnpj == "33333333333333" ||
                cnpj == "44444444444444" ||
                cnpj == "55555555555555" ||
                cnpj == "66666666666666" ||
                cnpj == "77777777777777" ||
                cnpj == "88888888888888" ||
                cnpj == "99999999999999")
                return false;

            // Valida DVs
            var tamanho = cnpj.length - 2
            var numeros = cnpj.substring(0, tamanho);
            var digitos = cnpj.substring(tamanho);
            var soma = 0;
            var pos = tamanho - 7;
            for (var i = tamanho; i >= 1; i--) {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;

            tamanho = tamanho + 1;
            numeros = cnpj.substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--) {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;

            return true;
        }

        function validaEmail(email) {
            var str = email;
            var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (filtro.test(str)) {
                return true;
            } else {
                return false;
            }
        }

        function validaMeAno(periodo) {
            periodo = periodo.split('/');
            if (periodo.length == 2
                && (periodo[0].length == 2 && periodo[1].length == 4)
                && (parseInt(periodo[0]) > 0 && parseInt(periodo[0]) <= 12)
                && (parseInt(periodo[1]) > 999 && parseInt(periodo[1]) <= 9999)) {
                return true;
            } else {
                return false;
            }
        }

        function validaDate(data) {
            /* @TODO - Amarrado a um único formato dd/mm/yyyy */
            var isoDateRe = new RegExp("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
            var matches = isoDateRe.exec(data);

            if (!matches) {
                return false;
            }

            var cDate = new Date(matches[3], (matches[2] - 1), matches[1]);

            var result = ((cDate.getDate() == matches[1]) &&
                          (cDate.getMonth() == (matches[2] - 1)) &&
                          (cDate.getFullYear() == matches[3]))

            return result;
        }

        function validaTime(time) {
            if (time == '00:00' || time == 0) {
                return false;
            }

            time = time.split(':');
            if (time.length >= 2) {
                if (time[0] == '' || time[0].length < 2 || parseInt(time[0]) >= 24) {
                    return false;
                }

                if (time[1] == '' || time[1].length < 2 || parseInt(time[1]) >= 60 || time[0] < 0) {
                    return false;
                }

                return true;
            }
        }

        function validaDatetime(datatime) {
            datatime = datatime.split(' ');
            if (datatime.length == 2) {
                return validaDate(datatime[0]) && validaTime(datatime[1]);
            }
        }

        function validaNumber(number, field) {
            number = toFloat(number);
            var result = !isNaN(number) && isFinite(number);

            var attrZero = false;
            if (vm.attrs.zero) {
                var valZero = field.attr("zero");
                //valor false para validar
                valZero = valZero == 'false' ? false : true;
                if (valZero) {
                    attrZero = true;
                }
            }

            if (result && number == 0 && !attrZero) {
                result = false;
            }

            return result;
        }

        function validaUrl(url) {
            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
            if (!regex.test(url)) {
                return false;
            } else {
                return true;
            }
        }

        function validaMinValue(valor, field) {
            return toFloat(valor) >= toFloat(field.attr(vm.attrs.minValue));
        }

        function validaMaxValue(valor, field) {
            return toFloat(valor) <= toFloat(field.attr(vm.attrs.maxValue));
        }

        function validaMinSelected(valor, field) {
            var min = parseInt(field.attr(vm.attrs.minSelected));
            var selecteds = angular.element(':checked', field.parent().parent()).length;
            return selecteds >= min;
        }

        function validaMaxSelected(valor, field) {
            var max = parseInt(field.attr(vm.attrs.maxSelected));
            var selecteds = angular.element(':checked', field.parent().parent()).length;
            return selecteds <= max;
        }
    };
}());;(function () {
    'use strict';

    angular
      .module('funcef-validate.directive')
      .directive('ngfValidate', NgfValidate);

    NgfValidate.$inject = ['$timeout'];

    /* @ngInject */
    function NgfValidate($timeout) {
        return {
            restrict: 'A',
            controller: 'NgfValidateController',
            controllerAs: 'vm',
            require: [
                '^name',
                '^model'
            ],
            scope: {
                name: '=',
                model: '='
            }
        };
    }
}());;