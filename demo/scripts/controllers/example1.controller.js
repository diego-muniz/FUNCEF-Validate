﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = ['$filter'];

    /* @ngInject */
    function Example1Controller($filter) {
        var vm = this;

        vm.selecionaTudo = selecionaTudo;
        vm.selecionaCheckbox = selecionaCheckbox;

		initArrays();
		init();

        function initArrays() {
            //delete vm.lists;
            vm.lists = {
                FiltroEntidadeOrigem: { column: 'Id', lista: [] },
            };
        } 

        function init(){
			initFiltroEntidade();        	
        }

        function initFiltroEntidade(){
        	vm.response = [
				{Id: 1, Descricao: "Combo 1"},
				{Id: 2, Descricao: "Combo 2"}
			];

            vm.lists.FiltroEntidadeOrigem.lista = vm.response;
            initCheckBox('FiltroEntidadeOrigem');
        }

        function initCheckBox(index){
        	angular.forEach(vm.lists[index].lista, function (item) {
                    item.checked = vm.pesquisa[index].indexOf(item[vm.lists[index].column]) >= 0;
                });
        }

		 function selecionaTudo(list) {
            angular.forEach(list.lista, function (item) {
                item.checked = list.checkAll;
            });
        }

        function selecionaCheckbox(list) {
        	 console.log(list);
             list.checkAll = list.length == $filter('filter')(list, { checked: true }).length;
        } 
    }
}());