﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-validate.controller', []);
    angular.module('funcef-validate.service', []);
    angular.module('funcef-validate.component', []);
    angular.module('funcef-validate.directive', []);

    angular
    .module('funcef-validate', [
      'funcef-validate.controller',
      'funcef-validate.service',
      'funcef-validate.component',
      'funcef-validate.directive'
    ]);
})();