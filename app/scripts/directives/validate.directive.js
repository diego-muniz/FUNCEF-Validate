﻿(function () {
    'use strict';

    angular
      .module('funcef-validate.directive')
      .directive('ngfValidate', NgfValidate);

    NgfValidate.$inject = ['$timeout'];

    /* @ngInject */
    function NgfValidate($timeout) {
        return {
            restrict: 'A',
            controller: 'NgfValidateController',
            controllerAs: 'vm',
            require: [
                '^name',
                '^model'
            ],
            scope: {
                name: '=',
                model: '='
            }
        };
    }
}());