# Componente - FUNCEF-Validate

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Validate possibilita a validação de campos.
 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-validate --save.
```

## Script

```html
<script src="bower_components/funcef-validate/dist/funcef-validate.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-validate/dist/funcef-validate.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funfef-header dentro do módulo do Sistema.

```js
angular
        .module('funcef-demo', ['funcef-validate']);
```

## Uso

O uso do mask não é obrigatório.

Para o uso do mask execute:
bower install funcef-mask --save -dev

```html
<form name="vm.form" novalidate model="vm.model" ngf-validate> 
    /*Nome*/
    <input name="nome" ng-model="vm.model.Nome"  required />
    
    /*Sobrenome*/
    <input name="sobrenome" ng-model="vm.model.Sobrenome" ng-required="vm.model.Nome" />
    
    /*CPF*/
    <input name="cpf" ng-model="vm.model.CPF" ngf-mask="cpf" required validator="cpf" />
    
    /*CNPJ*/
    <input name="cnpj" ng-model="vm.model.CNPJ" ngf-mask="cnpj" required validator="cnpj" />
    
    /*Email*/
    <input name="email" ng-model="vm.model.Email" required validator="email" />
    
    /*Data: (dd/mm/aaaa)*/
    <input name="data" ng-model="vm.model.Date" ngf-mask="data" required validator="date" />
    
    /*Data Hora: (dd/mm/aaaa hh:mm:ss)*/
    <input name="data" ng-model="vm.model.DataHora" ngf-mask="datahora" required validator="dateTime" />
    
    /*Hora: (hh:mm)*/
    <input name="hora" ng-model="vm.model.Hora" ngf-mask="hora" required validator="time" />
    
    /*Período: (mm/aaaa)*/
    <input name="mesano" ng-model="vm.model.MesAno" ngf-mask="mesano" required validator="mesAno" />
    
    /*Número*/
    <input name="numero" ng-model="vm.model.Numero" ngf-mask="numerico" required validator="number" />
    
    /*URL*/
    <input name="url" ng-model="vm.model.URL" required validator="url" />
    
    /*Campo de texto*/
    <textarea name="textarea" ng-model="vm.model.TextArea" required></textarea>
    
    /*Campo Rádio*/
    <input name="radio" ng-model="vm.model.Radio" required/>***
    
    /*Valida Rádio Button*/
    <input ng-model="vm.lists.FiltroEntidadeOrigem.checkAll" ng-click="vm.selecionaTudo(vm.lists.FiltroEntidadeOrigem)" value="0">****
    
    /*Marcar Todos*/
    <input ng-model="item.checked" ng-change="vm.selecionaCheckbox(vm.lists.FiltroEntidadeOrigem.lista)" name="chkEntidadeOrigem{{item.Id}}">*****
    
    /*Validação customizada*/
    <input name="custom" ng-model="vm.model.Custom" required />
</form>
```

### Parâmetros adicionais:

- name ();
- model ();

## Desinstalação:

```
bower uninstall funcef-validate --save
```
